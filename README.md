Movement Internet Timeline
============

The Movement Internet Timeline is a project developed by the [Progressive
Technology Project](https://progressivetech.org/) and [May First Movement
Technology](https://mayfirst.coop/) to inspire discussion and understanding of
the importance of the relationship between the movement and the Internet.


It's developed using the [impress.js](https://github.com/impress/impress.js/)
presentation framework.
